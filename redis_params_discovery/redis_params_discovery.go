package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"regexp"
	"strings"
    "os"
)

type zabbix_val struct {
	RedisKeyVal string `json:"{#REDISKEY}"`
}

type response_data struct {
	Data []zabbix_val `json:"data"`
}

func main() {

	redisHostPtr := flag.String("redis_host", "localhost", "Redis hostname")
	redisPassPtr := flag.String("redis_password", "", "Redis password")

	var redisHost string
	if os.Getenv("REDIS_HOST") != "" {
		redisHost = os.Getenv("REDIS_HOST")
	} else {
		redisHost = *redisHostPtr
	}

	var redisPass string
	if os.Getenv("REDIS_PASSWORD") != "" {
		redisPass = os.Getenv("REDIS_PASSWORD")
	} else {
		redisPass = *redisPassPtr
	}

	client := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":6379",
		Password: redisPass,
		DB:       0, // use default DB
	})

	val, err := client.Do("INFO").Result()
	if err != nil {
		panic(err)
	}

	var zabbix_vals []zabbix_val

	for _, line := range strings.Split(strings.TrimSuffix(val.(string), "\n"), "\n") {
		r, _ := regexp.Compile("^.+:.+$")
		if r.MatchString(line) {
			split_line := strings.Split(line, ":")
			line_val := zabbix_val{RedisKeyVal: split_line[0]}
			zabbix_vals = append(zabbix_vals, line_val)
		}
	}

	res := &response_data{Data: zabbix_vals}
	res_json, _ := json.Marshal(res)
	fmt.Println(string(res_json))
}
