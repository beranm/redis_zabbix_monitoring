build_redis_params_discovery:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go get github.com/go-redis/redis
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o zabbix_client_image/params_discovery redis_params_discovery/redis_params_discovery.go

build_redis_key:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go get github.com/go-redis/redis
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o zabbix_client_image/get_key_val redis_get_key_val/redis_get_key_val.go
