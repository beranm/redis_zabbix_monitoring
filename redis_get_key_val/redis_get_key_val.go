package main

import (
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"os"
	"regexp"
	"strings"
)

func main() {

	redisHostPtr := flag.String("redis_host", "localhost", "Redis hostname")
	redisPassPtr := flag.String("redis_password", "", "Redis password")
	redisKeyPtr := flag.String("redis_info_key", "redis_version", "Redis key from INFO command to get")

	var redisHost string
	if os.Getenv("REDIS_HOST") != "" {
		redisHost = os.Getenv("REDIS_HOST")
	} else {
		redisHost = *redisHostPtr
	}

	var redisPass string
	if os.Getenv("REDIS_PASSWORD") != "" {
		redisPass = os.Getenv("REDIS_PASSWORD")
	} else {
		redisPass = *redisPassPtr
	}

	flag.Parse()
	client := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":6379",
		Password: redisPass, // no password set
		DB:       0,         // use default DB
	})

	val, err := client.Do("INFO").Result()
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(strings.TrimSuffix(val.(string), "\n"), "\n") {
		r, _ := regexp.Compile("^.+:.+$")
		if r.MatchString(line) {
			split_line := strings.Split(line, ":")
			if split_line[0] == *redisKeyPtr {
				fmt.Println(split_line[1])
				break
			}
		}
	}
}
