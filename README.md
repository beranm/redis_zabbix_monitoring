#  Zabbix Redis Monitoring Autodiscovery image


GO binaries env variables

- `REDIS_HOST` -- hostname of Redis server
- `REDIS_PASSWORD` -- password to redis if any


Zabbix client expose following keys:

- `redis.discovery.params` -- this discoveres all the params from Redis INFO command with `{#REDISKEY}` macro name
- `redis.get.key[*]` -- given param at $1 gets a value by a param 


